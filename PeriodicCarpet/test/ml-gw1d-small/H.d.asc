# 1D ASCII output created by CarpetIOASCII
# created on nvidia by eschnetter on Jul 23 2015 at 21:50:37-0400
# parameter filename: "/xfs1/eschnetter/simulations/testsuite-nvidia-Cvanilla-sim-procs000002/output-0000/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# H d (H)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	-9.39991530662899e-07
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	-5.88491520346044e-07
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	-2.31519232217673e-07
0	0 0 0 0	3 3 3	0	-1 0 0	1.20905881229274e-07
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	4.5987145868856e-07
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	7.7777997160813e-07
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	1.06843430648966e-06
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	-1.03397401968336e-06
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	-6.83044181181331e-07
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	-3.23681193306732e-07
1	0 0 0 0	3 3 3	0.0125	-1 0 0	3.39498708183451e-08
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	3.79874510180512e-07
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	7.0700069497697e-07
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	1.00647152026379e-06
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	-1.12721435558267e-06
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	-7.77780059330507e-07
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	-4.16744266554145e-07
2	0 0 0 0	3 3 3	0.025	-1 0 0	-5.45805364535321e-08
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	2.97963064925997e-07
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	6.33807538310543e-07
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	9.42196593194477e-07
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	-1.2194675368227e-06
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	-8.7249821997149e-07
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	-5.10530836746749e-07
3	0 0 0 0	3 3 3	0.0375	-1 0 0	-1.44551449453382e-07
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	2.14267706696914e-07
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	5.58210911613709e-07
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	8.7568211529893e-07
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	-1.31049858390519e-06
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	-9.66981488976526e-07
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	-6.04865046486642e-07
4	0 0 0 0	3 3 3	0.05	-1 0 0	-2.35802682522595e-07
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	1.28892861477285e-07
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	4.80283188235029e-07
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	8.06951265863834e-07
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


