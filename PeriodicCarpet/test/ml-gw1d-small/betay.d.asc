# 1D ASCII output created by CarpetIOASCII
# created on nvidia by eschnetter on Jul 23 2015 at 21:50:37-0400
# parameter filename: "/xfs1/eschnetter/simulations/testsuite-nvidia-Cvanilla-sim-procs000002/output-0000/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# betay d (betay)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	0
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	0
0	0 0 0 0	3 3 3	0	-1 0 0	0
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	0
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	0
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	0
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	0
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	0
1	0 0 0 0	3 3 3	0.0125	-1 0 0	0
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	0
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	0
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	0
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	0
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	0
2	0 0 0 0	3 3 3	0.025	-1 0 0	0
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	0
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	0
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	0
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	0
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	0
3	0 0 0 0	3 3 3	0.0375	-1 0 0	0
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	0
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	0
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	0
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	0
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	0
4	0 0 0 0	3 3 3	0.05	-1 0 0	0
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	0
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	0
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


