# 1D ASCII output created by CarpetIOASCII
# created on nvidia by eschnetter on Jul 23 2015 at 21:50:32-0400
# parameter filename: "/xfs1/eschnetter/simulations/testsuite-nvidia-Cvanilla-sim-procs000002/output-0000/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small-amr.par"
#
# At11 y (At11)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0	0 0 0	46 0 6	0	0 -0.15 0	-0.0104719755089633
0	0	0 0 0	46 2 6	0	0 -0.1 0	-0.0104719755089633
0	0	0 0 0	46 4 6	0	0 -0.05 0	-0.0104719755089633
0	0	0 0 0	46 6 6	0	0 0 0	-0.0104719755089633
0	0	0 0 0	46 8 6	0	0 0.05 0	-0.0104719755089633
0	0	0 0 0	46 10 6	0	0 0.1 0	-0.0104719755089633
0	0	0 0 0	46 12 6	0	0 0.15 0	-0.0104719755089633

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0	0 1 0	46 0 6	0	0 -0.15 0	-0.0104719755089633
0	0	0 1 0	46 2 6	0	0 -0.1 0	-0.0104719755089633
0	0	0 1 0	46 4 6	0	0 -0.05 0	-0.0104719755089633
0	0	0 1 0	46 6 6	0	0 0 0	-0.0104719755089633
0	0	0 1 0	46 8 6	0	0 0.05 0	-0.0104719755089633
0	0	0 1 0	46 10 6	0	0 0.1 0	-0.0104719755089633
0	0	0 1 0	46 12 6	0	0 0.15 0	-0.0104719755089633


# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#

# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#

# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0	0 0 0	46 0 6	0.0125	0 -0.15 0	-0.0104645852192843
2	0	0 0 0	46 2 6	0.0125	0 -0.1 0	-0.0104645852192843
2	0	0 0 0	46 4 6	0.0125	0 -0.05 0	-0.0104645852192843
2	0	0 0 0	46 6 6	0.0125	0 0 0	-0.0104645852192843
2	0	0 0 0	46 8 6	0.0125	0 0.05 0	-0.0104645852192843
2	0	0 0 0	46 10 6	0.0125	0 0.1 0	-0.0104645852192843
2	0	0 0 0	46 12 6	0.0125	0 0.15 0	-0.0104645852192843

# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0	0 1 0	46 0 6	0.0125	0 -0.15 0	-0.0104645852192843
2	0	0 1 0	46 2 6	0.0125	0 -0.1 0	-0.0104645852192843
2	0	0 1 0	46 4 6	0.0125	0 -0.05 0	-0.0104645852192843
2	0	0 1 0	46 6 6	0.0125	0 0 0	-0.0104645852192843
2	0	0 1 0	46 8 6	0.0125	0 0.05 0	-0.0104645852192843
2	0	0 1 0	46 10 6	0.0125	0 0.1 0	-0.0104645852192843
2	0	0 1 0	46 12 6	0.0125	0 0.15 0	-0.0104645852192843


# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#

# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#

# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0	0 0 0	46 0 6	0.025	0 -0.15 0	-0.0104410555783212
4	0	0 0 0	46 2 6	0.025	0 -0.1 0	-0.0104410555783212
4	0	0 0 0	46 4 6	0.025	0 -0.05 0	-0.0104410555783212
4	0	0 0 0	46 6 6	0.025	0 0 0	-0.0104410555783212
4	0	0 0 0	46 8 6	0.025	0 0.05 0	-0.0104410555783212
4	0	0 0 0	46 10 6	0.025	0 0.1 0	-0.0104410555783212
4	0	0 0 0	46 12 6	0.025	0 0.15 0	-0.0104410555783212

# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0	0 1 0	46 0 6	0.025	0 -0.15 0	-0.0104410555783212
4	0	0 1 0	46 2 6	0.025	0 -0.1 0	-0.0104410555783212
4	0	0 1 0	46 4 6	0.025	0 -0.05 0	-0.0104410555783212
4	0	0 1 0	46 6 6	0.025	0 0 0	-0.0104410555783212
4	0	0 1 0	46 8 6	0.025	0 0.05 0	-0.0104410555783212
4	0	0 1 0	46 10 6	0.025	0 0.1 0	-0.0104410555783212
4	0	0 1 0	46 12 6	0.025	0 0.15 0	-0.0104410555783212


# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#

