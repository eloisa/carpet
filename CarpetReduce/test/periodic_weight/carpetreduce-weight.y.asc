# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Jun 24 2013 at 10:56:02-0400
# parameter filename: "../../arrangements/Carpet/CarpetReduce/test/periodic_weight.par"
#
# CARPETREDUCE::WEIGHT y (carpetreduce::weight)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:weight
0	0	0 0 0	13 0 13	0	0 -2.6 0	0
0	0	0 0 0	13 1 13	0	0 -2.4 0	0
0	0	0 0 0	13 2 13	0	0 -2.2 0	0
0	0	0 0 0	13 3 13	0	0 -2 0	1
0	0	0 0 0	13 4 13	0	0 -1.8 0	1
0	0	0 0 0	13 5 13	0	0 -1.6 0	1
0	0	0 0 0	13 6 13	0	0 -1.4 0	1
0	0	0 0 0	13 7 13	0	0 -1.2 0	1
0	0	0 0 0	13 8 13	0	0 -1 0	1
0	0	0 0 0	13 9 13	0	0 -0.8 0	1
0	0	0 0 0	13 10 13	0	0 -0.6 0	1
0	0	0 0 0	13 11 13	0	0 -0.4 0	1
0	0	0 0 0	13 12 13	0	0 -0.2 0	1
0	0	0 0 0	13 13 13	0	0 0 0	1
0	0	0 0 0	13 14 13	0	0 0.2 0	1
0	0	0 0 0	13 15 13	0	0 0.4 0	1
0	0	0 0 0	13 16 13	0	0 0.6 0	1
0	0	0 0 0	13 17 13	0	0 0.8 0	1
0	0	0 0 0	13 18 13	0	0 1 0	1
0	0	0 0 0	13 19 13	0	0 1.2 0	1
0	0	0 0 0	13 20 13	0	0 1.4 0	1
0	0	0 0 0	13 21 13	0	0 1.6 0	1
0	0	0 0 0	13 22 13	0	0 1.8 0	1
0	0	0 0 0	13 23 13	0	0 2 0	0
0	0	0 0 0	13 24 13	0	0 2.2 0	0
0	0	0 0 0	13 25 13	0	0 2.4 0	0


